
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var baseUrlVreme = 'http://api.openweathermap.org/data/';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


var koeficientNeugodnostiVremena = [0,0,0,0];
var koeficientMoznostiObiskaNarave = [0,0,0,0];
var koeficientObolenja = 0;


var generBool = 0;
var generiraniPacientiTabela = [];

var podatkiMeritev = [];

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 * case 1--> deli z pacientom 1 ki ima 1.ehrId
 * case 2--> deli s pacientom 2 ki ima 2.ehrId
 * Zelo bolan bolnik: 5463b200-c59d-4e89-8b0d-e35ac9dfd8ed  --> Pujsek Pepa
 * Zelo Zdrav bolnik: 1507a2cc-4cab-4308-9ee0-292c1957e826 --> Jure Beric
 * Rahlo bolan bolnik: 07bc3950-bff7-4721-ac29-f84c8dab0d85 --> Biggie Smalls
 * testing, attenition please: 0a4af513-7f88-4500-bf27-fec3f1521732
 * 
 */
function generirajPodatke(stPacienta) {
	
	if(generBool < 3){
		generBool ++;
		sessionId = getSessionId();
		ehrId = "";
		
		switch(stPacienta){
			case(1):
				ehrId = "5463b200-c59d-4e89-8b0d-e35ac9dfd8ed";
				break;
			case(2):
				ehrId = "1507a2cc-4cab-4308-9ee0-292c1957e826";
				break;
			case(3):
				ehrId = "07bc3950-bff7-4721-ac29-f84c8dab0d85";
				break;
				
		}
		
		generiraniPacientiTabela[stPacienta-1] = ehrId;
		
				$.ajax({
				url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
		    	success: function (data) {
					var party = data.party;
					$("#izbiranjeBolnika").append('<option value="' + ehrId + "|" + party.firstNames + "|" + party.lastNames + "|" + party.lastNames + "|" + party.dateOfBirth + '">' + party.firstNames + " " + party.lastNames + "</option>");
				},
				error: function(err) {
					console.log("err generiranje");
				}
			});
	
	  
	
		return ehrId;
	}
}



function funKoeficientObolenja(callback) {
	sessionId = getSessionId();
	
	var telesnaTemperatura = 0;
    var sistolicniKrvniTlak = 0;
    var diastolicniKrvniTlak = 0;
    koeficientObolenja = 0;

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		console.log("Prosim vnesite zahtevan podatek!");
	} else {
		
		
		$.ajax({
			url: baseUrl + "/view/" + ehrId + "/body_temperature",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				telesnaTemperatura = data[0].temperature;
				
							//temperatura
						    switch(true){
						        case (telesnaTemperatura <= 31):
						            koeficientObolenja += 25;
						            break;
						        case (telesnaTemperatura <= 34):
						            koeficientObolenja += 10;
						            break;
						        case (telesnaTemperatura <= 36):
						            koeficientObolenja += 3;
						            break;
						        case (telesnaTemperatura <= 38):
						            koeficientObolenja -= 3;
						            break;
						        case (telesnaTemperatura <= 38.5):
						            koeficientObolenja += 3;
						            break;
						        case(telesnaTemperatura <= 39.8):
						            koeficientObolenja += 10;
						            break;
						        case(39.8 < telesnaTemperatura):
						            koeficientObolenja += 25;
						            break;
						    	default:
						    		console.log("error na switchih");
						    }
				
			},
			error: function(err) {
				console.log("err body_temperature");
			}
		});
		
		
		
		$.ajax({
			url: baseUrl + "/view/" + ehrId + "/blood_pressure",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				sistolicniKrvniTlak = data[0].systolic;
				diastolicniKrvniTlak = data[0].diastolic;
				
					    //sistolicni tlak
					    switch(true){
					    	case (sistolicniKrvniTlak <= 70):
					    		koeficientObolenja += 6;
					    		break;
					    	case(sistolicniKrvniTlak <= 88):
					    		koeficientObolenja += 3;
					    		break;
					    	case(sistolicniKrvniTlak <= 135):
					    		koeficientObolenja -= 2;
					    		break;
					    	case (sistolicniKrvniTlak <= 155):
					    		koeficientObolenja += 3;
					    		break;
					    	case (sistolicniKrvniTlak > 155):
					    		koeficientObolenja += 6;
					    		break;
					    	default:
					    		console.log("error na switchih");	    		
					    }
					    
					    //dialostični tlak
					    switch(true){
					    	case(diastolicniKrvniTlak <= 35):
					    		koeficientObolenja += 6;
					    		break;
					    	case(diastolicniKrvniTlak <= 58):
					    		koeficientObolenja += 3;
					    		break;
					    	case(diastolicniKrvniTlak <= 92):
					    		koeficientObolenja -= 3;
					    		break;	    	
					    	case(diastolicniKrvniTlak <= 122):
					    		koeficientObolenja += 3;
					    		break;
					    	case(diastolicniKrvniTlak > 122):
					    		koeficientObolenja += 6;
					    		break; 
					    	default:
					    		console.log("error na switchih");	    		
					    }
				
			},
			error: function(err) {
				console.log("err Tlak");
			}
		});


	}
	
}



function funKoeficientNeugodnostiVremena(){
	sessionId = getSessionId();
	var mestoBolnika = $("#preberiImeMesta").val();
	koeficientNeugodnostiVremena = [0,0,0,0];
	
			$.ajax({
			url: baseUrlVreme + "2.5/forecast?q=" + mestoBolnika + "&units=metric&appid=20564302b8730841bea2fb0e3cadfaf1",
			type: 'GET',
			dataType: 'jsonp',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				
						for(var i=0;i < 5; i++){
							var dnevnoVremeMain = data.list[i*8].main;
							var trenutniKoefSlabegaVremena = 0;
							
							switch(true){
								case (dnevnoVremeMain.temp <= -15):
									koeficientNeugodnostiVremena[i] += 15;
									break;
								case (dnevnoVremeMain.temp <= -3):
									koeficientNeugodnostiVremena[i] += 7;
									break; 	
								case (dnevnoVremeMain.temp <= 13):
									koeficientNeugodnostiVremena[i] += 3;
									break;
								case (dnevnoVremeMain.temp <= 17):
									koeficientNeugodnostiVremena[i] += 0;
									break;
								case (dnevnoVremeMain.temp <= 21):
									koeficientNeugodnostiVremena[i] -= 1;
									break;
								case (dnevnoVremeMain.temp <= 24):
									koeficientNeugodnostiVremena[i] -= 5;
									break;
								case (dnevnoVremeMain.temp <= 29):
									koeficientNeugodnostiVremena[i] -= 2;
									break;									
								case (dnevnoVremeMain.temp <= 35):
									koeficientNeugodnostiVremena[i] += 3;
									break;									
								case (dnevnoVremeMain.temp > 35):
									koeficientNeugodnostiVremena[i] += 11;
									break;	
							}
							
							switch(true){
								case(dnevnoVremeMain.humidity <= 30):
									koeficientNeugodnostiVremena[i] += 4;
									break;
								case(dnevnoVremeMain.humidity <= 70):
									koeficientNeugodnostiVremena[i] -= 2;
									break;
								case(dnevnoVremeMain.humidity > 70):
									koeficientNeugodnostiVremena[i] += 3;
									break;								
							}
							

						}
						
						

			},
			error: function(err) {
				console.log("err vreme");
			}
		});
		
}


function izracunajKoeficienta(){
	koeficientMoznostiObiskaNarave = [0,0,0,0];
	funKoeficientObolenja();
	funKoeficientNeugodnostiVremena();
	prikazPodatkeOehrIdju();
	
	setTimeout(function racunanjeMoznostiObiskaNarave() {
		
		for(var j=0; j<4; j++){
			koeficientMoznostiObiskaNarave[j] = koeficientObolenja + koeficientNeugodnostiVremena[j];
			podatkiMeritev[j] = {
				'x': j,
				'y': -koeficientMoznostiObiskaNarave[j] + 35,
			};
		}
	}, 1800);
	
}


function prikazPodatkeOehrIdju(){
	ehrId = document.getElementById("preberiEHRid").value;
	
	
			$.ajax({
				url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
		    	success: function (data) {
					var party = data.party;
					document.getElementById('vecPodatkovOBolniku').innerHTML = "";
					$('#vecPodatkovOBolniku').append(
						"<h4>Vec podatkov o bolniku</h3>"+
						"<h5>EhrId:  " + ehrId + "</h5>" +
						"<h5>Ime:  " + party.firstNames + "</h5>" +
						"<h5>Priimerk:  " + party.lastNames + "</h5>" +
						"<h5>Datum rojstva:  " + party.dateOfBirth + "</h5>" +
						'<div>	<p style="margin-top:1%">S klikom na gumb "Nariši Graf" izrišemo  graf, ki nam za naslednje 3 dni(Danes:0, Jutri:1, Pojutrijšnjem:2...) izpiše,<br>kako koristen bi bil za nas (od 1 do 50) glede na naše zdravje in vreme, odhod na prosto ali v naravo.<br>(Najprej je potrebno zagnati aplikacijo in nato počakati nekaj sekund, preden lahko izrišemo graf) </p>' +
						'<button class="btn btn-primary btn-xs" onclick="risanjeGrafa()">Nariši Graf</button>' +
							'</div>' 
						);
					$('#insertGraphHere').append(
						"<div>" +
							'<div id="graf" style="margin-top = 0%;background-color: #ccf5ff">'+
							'</div>'+
						'</div>'
						
						);
				},
				error: function(err) {
					console.log("err generiranje");
				}
			});
}





/*
sistolični krvni tlak – tlak, ko srce potisne kri po žili; tlak manšete je ravno enak tlaku v žili, trenutek pred tem je žila še povsem stisnjena skupaj in v njej ni pretoka krvi; normalno 110–140 mmHg
diastolični krvni tlak – tlak, ko srce počiva in nastopi tišina; manšeta takrat žile ne stiska več; normalno 60–90 mmHg (9,3–12 kPa)
Normalna temperatura: 36.5 - 37.5
*/


function risanjeGrafa() {
	
	document.getElementById('graf').innerHTML = "";
	$('#graf').append('<svg id="vizual" width="1000" height="500"></svg>');
	
  var vizualizacija = d3.select("#vizual"),
    sirina = 1000,
    visina = 500,
    robovi = {
      top: 20,
      right: 20,
      bottom: 20,
      left: 50
    },
    xDolzina = d3.scale.linear().range([robovi.left, sirina - robovi.right]).domain([0,4]),

    yDolzina = d3.scale.linear().range([visina - robovi.top, robovi.bottom]).domain([0,50]),

    xOs = d3.svg.axis()
      .scale(xDolzina)
      .tickSize(5)
      .tickSubdivide(true),

    yOs = d3.svg.axis()
      .scale(yDolzina)
      .tickSize(5)
      .orient("left")
      .tickSubdivide(true);


  vizualizacija.append("svg:g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + (visina - robovi.bottom) + ")")
    .call(xOs);

  vizualizacija.append("svg:g")
    .attr("class", "y axis")
    .attr("transform", "translate(" + (robovi.left) + ",0)")
    .call(yOs);

  var linija = d3.svg.line()
  .x(function (d) {
    return xDolzina(d.x);
  })
  .y(function (d) {
    return yDolzina(d.y);
  })
  .interpolate('basis');

vizualizacija.append("svg:path")
  .attr("d", linija(podatkiMeritev))
  .attr("stroke", "blue")
  .attr("stroke-width", 2)
  .attr("fill", "none");

}



$(document).ready(function() {
	
	$('#izbiranjeBolnika').change(function() {
			var ehrId = "";
			var podatki = $(this).val().split("|");
			
			switch(podatki[1]){
				case("Pujsek"):
					document.getElementById("preberiEHRid").value = generiraniPacientiTabela[0];
					ehrId = generiraniPacientiTabela[0];
					break;
				case("Jure"):
					document.getElementById("preberiEHRid").value = generiraniPacientiTabela[1];
					ehrId = generiraniPacientiTabela[1];
					break;
				case("Biggie"):
					document.getElementById("preberiEHRid").value = generiraniPacientiTabela[2];
					ehrId = generiraniPacientiTabela[2];
					break;
			}
			
			
				//master/detail
			$.ajax({
				url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
		    	success: function (data) {
					var party = data.party;
					document.getElementById('vecPodatkovOBolniku').innerHTML = "";
					$('#vecPodatkovOBolniku').append(
						"<h4>Vec podatkov o bolniku</h3>"+
						"<h5>EhrId:  " + ehrId + "</h5>" +
						"<h5>Ime:  " + party.firstNames + "</h5>" +
						"<h5>Priimerk:  " + party.lastNames + "</h5>" +
						"<h5>Datum rojstva:  " + party.dateOfBirth + "</h5>" +
						'<div>	<p style="margin-top:1%">S klikom na gumb "Nariši Graf" izrišemo  graf, ki nam za naslednje 3 dni(Danes:0, Jutri:1, Pojutrijšnjem:2...) izpiše,<br>kako koristen bi bil za nas (od 1 do 50) glede na naše zdravje in vreme, odhod na prosto ali v naravo.<br>(Najprej je potrebno zagnati aplikacijo in nato počakati nekaj sekund, preden lahko izrišemo graf) </p>' +
						'<button class="btn btn-primary btn-xs" onclick="risanjeGrafa()">Nariši Graf</button>' +
							'</div>' 
						);
					$('#insertGraphHere').append(
						"<div>" +
							'<div id="graf" style="margin-top = 0%;background-color: #ccf5ff">'+
							'</div>'+
						'</div>'
						
						);	
				},
				error: function(err) {
					console.log("err generiranje");
				}
			});
			
			
			
			
		});
		
		

});